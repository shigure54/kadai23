import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Image;

public class FoodOrderMachine {
    private JPanel root;
    private JLabel Title1;
    private JLabel Title2;
    private JButton tempButton;
    private JButton karaButton;
    private JButton gyouButton;
    private JButton udonButton;
    private JButton HBGButton;
    private JButton ramenButton;
    private JTextPane ReceivedInfo;
    private JButton checkOutButton;
    private JLabel pricebox;
    private JLabel Tax;
    private JLabel taxbox;
    int totalprice = 0;
    double totalpriceincludetax = 0;


    public FoodOrderMachine() {
        tempButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",100);
            }
        });
        tempButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("Tempura.jpg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        karaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",200);
            }
        });
        karaButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("karaage.jpg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        gyouButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza",300);
            }
        });
        gyouButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("gyouza.jpg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",400);
            }
        });
        udonButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("udon.jpeg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        HBGButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger",500);
            }
        });
        HBGButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("hamburger.jpg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",600);
            }
        });
        ramenButton.setIcon(
                new ImageIcon(
                        new ImageIcon(this.getClass().getResource("ramen.jpg")).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)
                )
        );

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Thank you! The total price is " + String.format("%.1f",totalpriceincludetax) + "yen");
                totalprice= 0 ;
                totalpriceincludetax = 0;
                pricebox.setText("0 yen");
                taxbox.setText("0 yen");
                ReceivedInfo.setText(null);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food , int price) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + "?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String text = "Order for " + food + " received.";
            String currentText = ReceivedInfo.getText();;
            ReceivedInfo.setText(currentText + food + " "+ price + "yen" + "\n");
            JOptionPane.showMessageDialog(null, text);
            this.totalprice += price;
            this.totalpriceincludetax = totalprice * 1.1;
            pricebox.setText(this.totalprice + "yen");
            taxbox.setText(this.totalprice *0.1 + "yen");
        }

    }
}
